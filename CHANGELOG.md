
## 1.0.5 [10-15-2024]

* Changes made at 2024.10.14_21:15PM

See merge request itentialopensource/adapters/adapter-db_postgresql!13

---

## 1.0.4 [08-25-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-db_postgresql!11

---

## 1.0.3 [08-14-2024]

* Changes made at 2024.08.14_19:30PM

See merge request itentialopensource/adapters/adapter-db_postgresql!10

---

## 1.0.2 [08-07-2024]

* Changes made at 2024.08.07_10:52AM

See merge request itentialopensource/adapters/adapter-db_postgresql!9

---

## 1.0.1 [07-26-2024]

* manual migration updates

See merge request itentialopensource/adapters/persistence/adapter-db_postgresql!7

---

## 1.0.0 [01-07-2024]

* Major/2023 auto migration

See merge request itentialopensource/adapters/persistence/adapter-db_postgresql!4

---

## 0.2.1 [11-22-2019]

* fix pipeline

See merge request itentialopensource/adapters/persistence/adapter-db_postgresql!2

---

## 0.2.0 [11-22-2019]

* adding ssl support

See merge request itentialopensource/adapters/persistence/adapter-db_postgresql!1

---

## 0.1.3 [11-21-2019]

* Bug fixes and performance improvements

See commit 8ffc69e

---

## 0.1.2 [11-21-2019]

* Bug fixes and performance improvements

See commit 4b1b47f

---

## 0.1.1 [11-19-2019]

* Bug fixes and performance improvements

See commit 77e4b6c

---
