
## 0.2.1 [11-22-2019]

* fix pipeline

See merge request itentialopensource/adapters/persistence/adapter-db_postgresql!2

---

## 0.2.0 [11-22-2019]

* adding ssl support

See merge request itentialopensource/adapters/persistence/adapter-db_postgresql!1

---

## 0.1.3 [11-21-2019]

* Bug fixes and performance improvements

See commit 8ffc69e

---

## 0.1.2 [11-21-2019]

* Bug fixes and performance improvements

See commit 4b1b47f

---

## 0.1.1 [11-19-2019]

* Bug fixes and performance improvements

See commit 77e4b6c

---
