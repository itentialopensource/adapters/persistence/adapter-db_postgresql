PostgreSQL Adapter
===========

This adapter allows interaction with a PostgreSQL server. For efficiency, this adapter should only be used in IAP workflow calls. Calling the adapter from Applications instead of using the npm pg pacakge will be less efficient!

License & Maintainers
---

### Maintained by:

Itential Adapter Team (<product_team@itential.com>)

Check the [changelog](CHANGELOG.md) for the latest changes.

### License

Itential, LLC proprietary
