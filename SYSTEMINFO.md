# PostgreSQL

Vendor: PostgreSQL
Homepage: https://www.postgresql.org/

Product: PostgreSQL
Product Page: https://www.postgresql.org/

## Introduction
We classify PostgreSQL into the Data Storage domaina as PostgreSQL is a database which provides the storage of information. 

"The World's Most Advanced Open Source Relational Database"
"PostgreSQL is a powerful, open source object-relational database system with over 35 years of active development that has earned it a strong reputation for reliability, feature robustness, and performance."

## Why Integrate
The PostgreSQL adapter from Itential is used to integrate the Itential Automation Platform (IAP) with PostgreSQL. With this adapter you have the ability to perform operations with PostgreSQL on items such as:

- Storage of Information
- Retrieval of Information

## Additional Product Documentation
The [PostgreSQL SQL Reference](https://www.postgresqltutorial.com/)
The [PostgreSQL Node Library Documentation](https://www.npmjs.com/package/pg)